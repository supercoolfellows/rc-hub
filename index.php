<!-- 
* Author: Sean Watts TESTk
* Email: swatts@stevens.ed
* Date Created: 7/1/2015
-->

<?php
include ("includes/my_include.php");
session_start();

if(isset($_GET['logout']) && $_GET['logout'] == "true") {
	$_SESSION['email']="";
	session_destroy();
}
setTitle("RC Book");
addHeader ( "Welcome!" );
addParagraph ( "RC Book is a website that allows you to show off your radio controlled models. To get started create an account below." );
if(!isLoggedIn()){
addCreateAccountForm();
}
display ();
/*
 ******************** FUNCTIONS ********************
 */
function addCreateAccountForm() {
	global $tpl;
	$tpl->parse ( "BODY", ".createAccountForm" );
}
?>