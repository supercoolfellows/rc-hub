<!-- 
* Author: Sean Watts
* Email: swatts@stevens.edu
* Date Created: 7/26/2015
-->

<?php
include ("includes/my_include.php");
session_start ();

$row = getModelInfo ();
setTitle ( $row [1] );

if (isMyModel ()) {
	if (isset ( $_GET ['edit'] ) && $_GET ['edit'] == "about") {
		addViewMyModelInfoTabEditable ( $row [2], $row [3], $row [4] );
		setModelInfoActive ();
		addAllComponents ( - 1 );
	} else if (isset ( $_GET ['edit'] ) && $_GET ['edit'] == "components") {
		addViewMyModelInfoTab ( $row [2], $row [3], $row [4] );
		if (isset ( $_GET ['deletecomponent'] )) {
			deleteComponent ( $_GET ['component_id'] );
			addAllComponents ( - 1 );
		} else if (isset ( $_GET ['component_id'] )) {
			addAllComponents ( $_GET ['component_id'] );
			
			
		} else if (isset ( $_POST ['componentname'] ) && trim ( $_POST ['componentname'] ) != "") {
			if ($_POST ['componentname'] != "Select One") {
				if(addComponentToModel ( $_GET['id'] )){
					successMessage ( "All Done: ", "'" . $_POST ['componentname'] . "' has been added" );
				}else{
					errorMessage("Empty Component","The component you tried to add had no specs or features set");
				}
				setModelComponentsActive ();
			}			
			addAllComponents ( - 1 );
		} else {
			addAllComponents ( - 1 );
		}
		setModelComponentsActive ();
	} else {
		if (isset ( $_GET ['updatemodel'] ) && $_GET ['updatemodel'] == "true") {
			updateModel ();
			$row = getModelInfo ();
		}
		if (isset ( $_POST ["uploadImage"] )) {
			saveImg ();
		}
		updateModelComponent ();
		addViewMyModelInfoTab ( $row [2], $row [3], $row [4] );
		setModelInfoActive ();
		addAllComponents ( - 1 );
	}
	addViewMyModelNavTabs ();
} else if ($row != null) {
	addViewModelInfoTab ( $row [2], $row [3], $row [4] );
	addViewModelNavTabs ();
	setModelInfoActive ();
	addAllComponents ( - 1 );
} else {
	errorMessage ( "Model Not Found:", " The model you are looking for does not exist, it may have been deletd" );
	display ();
	exit ();
}
addHeader ( $row [1] );
display ();
/*
 * ******************* FUNCTIONS ********************
 */
function getModelInfo() {
	global $db;
	$query = "SELECT author_id, name, type, timestamp, description FROM models WHERE model_id = '" . $_GET ['id'] . "';";
	$res = $db->send_sql ( $query );
	return $db->next_row ();
}
function addViewModelInfoTab($type, $timestamp, $description) {
	global $tpl;
	$tpl->assign ( "VIEWMODELTYPE", $type );
	$tpl->assign ( "VIEWMODELPICTURE", getProfilePic () );
	$tpl->assign ( "VIEWMODELDESCRIPTION", $description );
	$tpl->parse ( "VIEWMODELINFOTAB", ".viewModelNavTabsInfoTab" );
}
function addViewMyModelInfoTab($type, $timestamp, $description) {
	global $tpl;
	$tpl->assign ( "VIEWMYMODELTYPE", $type );
	$tpl->assign ( "VIEWMYMODELID", $_GET ['id'] );
	$tpl->assign ( "VIEWMYMODELPICTURE", getProfilePic () );
	$tpl->assign ( "VIEWMYMODELDESCRIPTION", $description );
	$tpl->parse ( "VIEWMYMODELINFOTAB", ".viewModelNavTabsInfoTab" );
	$tpl->parse ("VIEWMODELINFOEDITBUTTON", ".viewModelInfoEditButton");
}
function addViewMyModelInfoTabEditable($type, $timestamp, $description) {
	global $tpl;
	$tpl->assign ( "VIEWMYMODELTYPE", $type );
	$tpl->assign ( "VIEWMYMODELID", $_GET ['id'] );
	$tpl->assign ( "VIEWMYMODELPICTURE", getProfilePic () );
	$tpl->assign ( "VIEWMYMODELDESCRIPTION", $description );
	$tpl->parse ( "VIEWMYMODELINFOTAB", ".viewModelNavTabsInfoTabEditable" );
	$tpl->assign ("VIEWMODELINFOEDITBUTTON", "");
}
function addComponent($name, $componentid, $editable) {
	global $tpl;
	$tpl->assign ( "VIEWMODELCOMPONENTNAME", $name );
	$tpl->assign ( "VIEWMODELCOMPONENTIMAGE", "./images/Components/" . str_replace(' ', '_', $name) . ".jpg" );
	$tpl->assign ( "VIEWMODELCOMPONENTID", $componentid );
	addFeatures ( $componentid, $editable );
	addSpecs ( $componentid, $editable );
	if (isMyModel () == true) {
		if ($editable == true) {
			$tpl->parse ( "VIEWMODELCOMPONENTSACCORDION", ".viewModelComponentEditable" );
		} else {
			$tpl->parse ( "VIEWMODELCOMPONENTSACCORDION", ".viewMyModelComponent" );
		}
	} else {
		$tpl->parse ( "VIEWMODELCOMPONENTSACCORDION", ".viewModelComponent" );
	}
}
function addSpecs($componentid, $editable) {
	global $tpl;
	global $db_user;
	global $db_password;
	global $db_host;
	global $db_name;
	$db = new database ();
	$db->setup ( $db_user, $db_password, $db_host, $db_name );
	$query = "SELECT spec_id, name, value FROM specs WHERE component_id = '" . $componentid . "';";
	$res = $db->send_sql ( $query );
	$tpl->parse ( "COMPONENTSPECS", "null" );
	if (mysqli_num_rows ( $res ) != 0) { // Check if component exist
		$tpl->assign ( "COMPONENTSPECSTABLE", "" );
		while ( $row = $db->next_row () ) {
			$tpl->assign ( "SPECID", $row [0] );
			$tpl->assign ( "SPECNAME", $row [1] );
			$tpl->assign ( "SPECVALUE", $row [2] );
			if ($editable == true) {
				$tpl->parse ( "COMPONENTSPECS", ".viewModelSpecEditable" );
			} else {
				$tpl->parse ( "COMPONENTSPECS", ".viewModelSpec" );
			}
		}
	}else{
		$tpl->assign ( "COMPONENTSPECSTABLE", "style=\"display:none\"" );
	}
}
function addFeatures($componentid, $editable) {
	global $tpl;
	global $db_user;
	global $db_password;
	global $db_host;
	global $db_name;
	$db = new database ();
	$db->setup ( $db_user, $db_password, $db_host, $db_name );
	$query = "SELECT feature_id, name FROM features WHERE component_id = '" . $componentid . "';";
	$res = $db->send_sql ( $query );
	$tpl->parse ( "COMPONENTFEATURES", "null" );
	if (mysqli_num_rows ( $res ) != 0) { // Check if component exist
		$tpl->assign ( "COMPONENTFEATURESTABLE", "" );
		while ( $row = $db->next_row () ) {
			$tpl->assign ( "FEATUREID", $row [0] );
			$tpl->assign ( "FEATURENAME", $row [1] );
			if ($editable == true) {
				$tpl->parse ( "COMPONENTFEATURES", ".viewModelFeatureEditable" );
			} else {
				$tpl->parse ( "COMPONENTFEATURES", ".viewModelFeature" );
			}
		}
	}else{
		$tpl->assign ( "COMPONENTFEATURESTABLE", "style=\"display:none\"" );
	}
}
function addAllComponents($editableComponent) {
	global $tpl;
	global $db_user;
	global $db_password;
	global $db_host;
	global $db_name;
	$db = new database ();
	$db->setup ( $db_user, $db_password, $db_host, $db_name );
	$query = "SELECT name, component_id, model_id FROM components WHERE model_id = '" . $_GET ['id'] . "';";
	$res = $db->send_sql ( $query );
	if (mysqli_num_rows ( $res ) != 0) {
	while ( $row = $db->next_row () ) {
		if ($row [1] == $editableComponent) {
			addComponent ( $row [0], $row [1], true );
		} else {
			addComponent ( $row [0], $row [1], false );
		}
	}
	$tpl->parse ( "VIEWMYMODELCOMPONENTSTAB", ".viewModelComponentsAccordion" );
	}else{
		$tpl->assign("VIEWMODELCOMPONENTSACCORDION","");
		infoMessage("No Components:", "You have not added any components to this model.");
	}
	$tpl->assign ( "COMPONENTFORMDESTINATION", "viewmodel.php?id=" . $_GET ['id'] . "&edit=components");
	if (isMyModel ()) {
		$tpl->parse ( "VIEWMYMODELCOMPONENTSTAB", ".componentsForm" );
	} else {
		$tpl->parse ( "VIEWMODELCOMPONENTSTAB", ".componentsForm" );
	}
}
function addViewModelNavTabs() {
	global $tpl;
	$tpl->parse ( "BODY", ".viewModelNavTabs" );
}
function addViewMyModelNavTabs() {
	global $tpl;
	$tpl->parse ( "BODY", ".viewMyModelNavTabs" );
}
function getProfilePic() {
	$row = getModelInfo ();
	$dir = "./UserData/" . $row [0] . "/model_" . $_GET ['id'] . "/profilepic/";
	$files = scandir ( $dir );
	foreach ( $files as $file ) {
		if (is_file ( $dir . $file )) {
			return $dir . $file;
		}
	}
	return "./images/emptyModel.jpg";
}
function updateInfo($table, $field, $newVal) {
	global $db;
	$sql = "UPDATE " . $table . " SET " . $db->escape ( $field ) . "='" . $db->escape ( $newVal ) . "' WHERE model_id='" . $db->escape ( $_GET ['id'] ) . "';";
	$res = $db->send_sql ( $sql );
}
function updateModel() {
	if (isset ( $_POST ['modelname'] ) && trim ( $_POST ['modelname'] ) != "") {
		updateInfo ( "models", "name", $_POST ['modelname'] );
	}
	if (isset ( $_POST ['modeltype'] ) && trim ( $_POST ['modeltype'] ) != "") {
		updateInfo ( "models", "type", $_POST ['modeltype'] );
	}
	if (isset ( $_POST ['modeldescription'] ) && trim ( $_POST ['modeldescription'] ) != "") {
		updateInfo ( "models", "description", $_POST ['modeldescription'] );
	}
}
function updateModelComponent() {
	global $db;
	foreach ( $_POST as $key => $value ) {
		$tokens = explode ( "_", $key );
		if (strcmp ( $tokens [0], "specName" ) == 0) {
			$sql = "UPDATE specs SET name ='" . $db->escape ( $value ) . "' WHERE spec_id='" . $db->escape ( $tokens [1] ) . "';";
			$res = $db->send_sql ( $sql );
		} else if (strcmp ( $tokens [0], "specValue" ) == 0) {
			$sql = "UPDATE specs SET value ='" . $db->escape ( $value ) . "' WHERE spec_id='" . $db->escape ( $tokens [1] ) . "';";
			$res = $db->send_sql ( $sql );
		} else if (strcmp ( $tokens [0], "featureName" ) == 0) {
			$sql = "UPDATE features SET name ='" . $db->escape ( $value ) . "' WHERE feature_id='" . $db->escape ( $tokens [1] ) . "';";
			$res = $db->send_sql ( $sql );
		}
	}
}
function saveImg() {
	$currentPic = getProfilePic ();
	$target_dir = "./UserData/" . $_SESSION ['id'] . "/model_" . $_GET ['id'] . "/profilepic/";
	$temp_name = $_FILES ["pic"] ["tmp_name"]; // get the temporary filename/path on the server
	$name = $_FILES ["pic"] ["name"]; // get the filename of the actual file
	$imageFileType = pathinfo ( $target_dir . $name, PATHINFO_EXTENSION );
	if ($_FILES ["pic"] ["size"] > 6000000) {
		errorMessage ( "File Size: ", "Sorry, the file you chose exceeds the 6Mb size limit." );
		return;
	}
	if (strtolower ( $imageFileType ) != "jpg" && strtolower ( $imageFileType ) != "png" && strtolower ( $imageFileType ) != "gif") {
		errorMessage ( "File Type: ", "Sorry, the file you chose is not a picture. Only JPG, PNG, and GIF formats are allowed." );
		return;
	}
	move_uploaded_file ( $temp_name, $target_dir . $name );
	if ($currentPic != "./UserData/images/emptyModel.jpg") {
		unlink ( $currentPic );
	}
}
function setModelInfoActive() {
	global $tpl;
	$tpl->assign ( "MODELINFOACTIVE", "active" );
	$tpl->assign ( "MODELINFOTABACTIVE", "class=\"active\"" );
	$tpl->assign ( "MODELCOMPONENTSACTIVE", "" );
	$tpl->assign ( "MODELCOMPONENTSTABACTIVE", "class=\"enabled\"" );
	$tpl->assign ( "MODELPICTURESACTIVE", "" );
	$tpl->assign ( "MODELPICTURESTABACTIVE", "class=\"disabled\"" );
	$tpl->assign ( "MODELVIDEOSACTIVE", "" );
	$tpl->assign ( "MODELVIDEOSTABACTIVE", "class=\"disabled\"" );
	$tpl->assign ( "MODELTUNINGACTIVE", "" );
	$tpl->assign ( "MODELTUNINGTABACTIVE", "class=\"disabled\"" );
	$tpl->assign ( "MODELSETTINGSACTIVE", "" );
	$tpl->assign ( "MODELSETTINGSTABACTIVE", "class=\"disabled\"" );
}
function setModelComponentsActive() {
	global $tpl;
	$tpl->assign ( "MODELINFOACTIVE", "" );
	$tpl->assign ( "MODELINFOTABACTIVE", "class=\"enabled\"" );
	$tpl->assign ( "MODELCOMPONENTSACTIVE", "active" );
	$tpl->assign ( "MODELCOMPONENTSTABACTIVE", "class=\"active\"" );
	$tpl->assign ( "MODELPICTURESACTIVE", "" );
	$tpl->assign ( "MODELPICTURESTABACTIVE", "class=\"disabled\"" );
	$tpl->assign ( "MODELVIDEOSACTIVE", "" );
	$tpl->assign ( "MODELVIDEOSTABACTIVE", "class=\"disabled\"" );
	$tpl->assign ( "MODELTUNINGACTIVE", "" );
	$tpl->assign ( "MODELTUNINGTABACTIVE", "class=\"disabled\"" );
	$tpl->assign ( "MODELSETTINGSACTIVE", "" );
	$tpl->assign ( "MODELSETTINGSTABACTIVE", "class=\"disabled\"" );
}
function isMyModel() {
	$row = getModelInfo ();
	if ($row [0] == $_SESSION ['id']) {
		return true;
	} else {
		return false;
	}
}
function deleteComponent($component_id) {
	global $db;
	$query = "SELECT component_id, model_id, name FROM components WHERE component_id = '" . $component_id . "';";
	$res = $db->send_sql ( $query );
	if (mysqli_num_rows ( $res ) != 0) { // Check if component exist
		$row = $db->next_row ();
		$componentName = $row [2];
		$query = "SELECT author_id FROM models WHERE model_id = '" . $row [1] . "';";
		$res = $db->send_sql ( $query );
		$row = $db->next_row ();
		if ($row [0] == $_SESSION ['id']) { // Check is component is from a model owned by user
			$query = "DELETE FROM components WHERE component_id = '" . $component_id . "';";
			$res = $db->send_sql ( $query );
			successMessage ( "Component Deleted: ", " '" . $componentName . "' has been deleted" );
		} else {
			errorMessage ( "HACKER: ", " You are attempting to delete a compoennt from a model that is not yours" );
		}
	} else {
		errorMessage ( "Error: ", " The component could not be found" );
	}
}
function addComponentToModel($model_id) {
	global $db;
	// Add new component
	$sql = "INSERT INTO components (model_id, name)
				VALUES ('" . $model_id . "', '" . $db->escape ( $_POST ['componentname'] ) . "');";
	$res = $db->send_sql ( $sql );
	$newComponentId = $db->insert_id ();
	$i = 1;
	$count = 0;
		while ( isset ( $_POST ['specName_' . $i] ) ) {
		if (isset ( $_POST ['specValue_' . $i] ) && trim ( $_POST ['specValue_' . $i] ) != "") {
			$sql = "INSERT INTO specs (`spec_id`, `component_id`, `name`, `value`)
				VALUES (NULL, '" . $newComponentId . "', '" . $_POST ['specName_' . $i] . "', '" . $_POST ['specValue_' . $i] . "');";
			$count++;
		}
		$i ++;
	}
	$i = 1;
	while ( isset ( $_POST ['featureName_' . $i] ) ) {
		if (isset ( $_POST ['featureName_' . $i] ) && trim ( $_POST ['featureName_' . $i] ) != "") {
			$sql = "INSERT INTO features (`feature_id`, `component_id`, `name`)
				VALUES (NULL, '" . $newComponentId . "', '" . $_POST ['featureName_' . $i] . "');";
			$res = $db->send_sql ( $sql );
			$count++;
		}
		$i ++;
	}
	if($count==0) {
		$sql = "DELETE FROM components
				WHERE component_id = '" . $newComponentId . "';";
		$res = $db->send_sql ( $sql );
		return false;
	}
	return true;
}
?>