/*
 * Author: Sean Watts
 * Email: swatts@stevens.edu
 * Date Created: 7/28/2015
 */
var specCounter = 0;
var featureCounter = 0;
var COMPONENT_IMAGES = "./images/Components/";

function incrementSpec() {
	specCounter += 1;
}

function incrementFeature() {
	featureCounter += 1;
}

function resetElements() {
	document.getElementById('submitbtn').style.display="none";
	document.getElementById('hrsubmit').style.display="none";
	document.getElementById('myForm').innerHTML = '';
}

function addSpec($name) {
	incrementSpec();
	var inputGrpParent = document.createElement('div');
	var inputGrpName = document.createElement('div');
	var inputGrpValue = document.createElement('div');
	var specName = document.createElement("INPUT");
	var specValue = document.createElement("INPUT");
	inputGrpParent.setAttribute("class", "well col-xs-6");
	inputGrpParent.setAttribute("id", "specInputGrpParentId_" + specCounter);

	inputGrpName.setAttribute("class", "form-group span2");
	inputGrpName.setAttribute("id", "specInputGrpNameId_" + specCounter);
	inputGrpValue.setAttribute("class", "form-group span2");
	inputGrpValue.setAttribute("type", "number");
	inputGrpValue.setAttribute("id", "specInputGrpValueId_" + specCounter);

	specName.setAttribute("type", "text");
	specName.setAttribute("placeholder", "Name");
	specName.setAttribute("class", "form-control input-group-lg reg_name");
	specName.setAttribute("Name", "specName_" + specCounter);
	if ($name != "") {
		specName.value = $name;
	}
	inputGrpName.appendChild(specName);
	inputGrpValue.appendChild(specValue);

	specValue.setAttribute("type", "number");
	specValue.setAttribute("placeholder", "Value");
	specValue.setAttribute("class", "form-control input-group-lg reg_name");
	specValue.setAttribute("step", "0.01");
	specValue.setAttribute("Name", "specValue_" + specCounter);

	inputGrpParent.appendChild(inputGrpName);
	inputGrpParent.appendChild(inputGrpValue);
	document.getElementById("myForm").appendChild(inputGrpParent);
}

function addFeature($placeholder) {
	incrementFeature();
	var inputGrpParent = document.createElement('div');
	var inputGrp = document.createElement('div');
	var featureName = document.createElement("INPUT");
	inputGrpParent.setAttribute("class", "well col-xs-6");
	inputGrpParent.setAttribute("id", "featureInputGrpParentId_"
			+ featureCounter);
	inputGrp.setAttribute("class", "form-group span2");
	inputGrp.setAttribute("id", "featureInputGrpId_" + featureCounter);

	featureName.setAttribute("type", "text");
	featureName.setAttribute("placeholder", "Feature Name");
	featureName.setAttribute("class", "form-control");
	featureName.setAttribute("Name", "featureName_" + featureCounter);
	if ($placeholder != "") {
		featureName.setAttribute("placeholder", $placeholder);
	}
	inputGrp.appendChild(featureName);

	inputGrpParent.appendChild(inputGrp);
	document.getElementById("myForm").appendChild(inputGrpParent);
}

function addImage($component) {
	var Parent = document.createElement('div');
	var img = document.createElement('img');
	Parent.setAttribute("id", "imgParent");
	img.setAttribute("id", "imgId");
	img.setAttribute("src", $component);
	Parent.appendChild(img);
	document.getElementById("myForm").appendChild(Parent);
	document.getElementById('hrsubmit').style.display="block";
	document.getElementById('submitbtn').style.display="block";
}

function selectionMade() {
	resetElements();
	if (document.getElementById("componentname").value == "Battery") {
		addImage(COMPONENT_IMAGES + "battery.jpg");
		addSpec("Capacity (MaH)");
		addSpec("Cell Count")
		addSpec("C Continous");
		addSpec("C Burst");
	} else if (document.getElementById("componentname").value == "(ESC) Electronic Speed Controller") {
		addImage(COMPONENT_IMAGES + "(esc)_electronic_speed_controller.jpg");
		addSpec("Amperage (A)");
		addFeature("Firmware");
	} else if (document.getElementById("componentname").value == "Frame") {
		addImage(COMPONENT_IMAGES + "frame.jpg");
		addSpec("Size (mm)");
		addFeature("Material");
	} else if (document.getElementById("componentname").value == "Flight Controller") {
		addImage(COMPONENT_IMAGES + "flight_controller.jpg");
		addFeature("Name/Type");
		addFeature("Firmware");
		addFeature("On Board Compass");
		addFeature("On Board Barometer");
	} else if (document.getElementById("componentname").value == "GPS Module") {
		addImage(COMPONENT_IMAGES + "gps_module.jpg");
		addFeature("Name/Type");
		addSpec("Refresh Frequency (Hz)");
	} else if (document.getElementById("componentname").value == "Motor") {
		addImage(COMPONENT_IMAGES + "motor.jpg");
		addFeature("Name/Type");
		addSpec("KV");
		addSpec("Pole Count");
	} else if (document.getElementById("componentname").value == "Propeller") {
		addImage(COMPONENT_IMAGES + "propeller.jpg");
		addSpec("Length (inches)");
		addSpec("Pitch");
		addFeature("Material");
	}else if (document.getElementById("componentname").value == "Receiver") {
		addImage(COMPONENT_IMAGES + "receiver.jpg");
		addSpec("Number of Channels");
		addFeature("Name");
	}else if (document.getElementById("componentname").value == "Camera") {
		addImage(COMPONENT_IMAGES + "camera.jpg");
		addSpec("Framerate (FPS)");
		addSpec("Resolution");
	}else if (document.getElementById("componentname").value == "Servo") {
		addImage(COMPONENT_IMAGES + "servo.jpg");
		addSpec("Speed (4.8V/6.0V)");
		addSpec("Torque oz./in");
	}else if (document.getElementById("componentname").value == "FPV Camera") {
		addImage(COMPONENT_IMAGES + "fpv_camera.jpg");
		addSpec("TVL");
	}else if (document.getElementById("componentname").value == "FPV Transmitter") {
		addImage(COMPONENT_IMAGES + "fpv_transmitter.jpg");
		addSpec("Output Power (mW)");
	}

	if (document.getElementById("componentname").value == "Select One") {
		document.getElementById('btnAdd').disabled = true;
	} else {
		document.getElementById('btnAdd').disabled = false;
	}
}