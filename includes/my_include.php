<!-- 
* Author: Sean Watts
* Email: swatts@stevens.edu
* Date Created: 7/1/2015
-->

<?php
include ("./includes/databaseClassMySQLi.php");
include ("./includes/databaseConnection.php");
include ("./includes/class.FastTemplate.php");
$tpl = new FastTemplate ( "./TEMPLATES" );
$tpl->define ( array (
		"main" => "main.html",
		
		// ***********************************
		// My Account
		// ***********************************
		"myAccountNavTabs" => "MyAccount/myAccountNavTabs.html",
		"myAccountNavTabsLocationTab" => "MyAccount/myAccountNavTabsLocationTab.html",
		"myAccountNavTabsLocationTabEditable" => "MyAccount/myAccountNavTabsLocationTabEditable.html",
		"myAccountNavTabsAccountTab" => "MyAccount/myAccountNavTabsAccountTab.html",
		"myAccountNavTabsAccountTabEditable" => "MyAccount/myAccountNavTabsAccountTabEditable.html",
		"myAccountModelLink" => "MyAccount/myAccountModelLink.html",
		"myAccountModelAccordion" => "MyAccount/myAccountModelAccordion.html",
		"newModelNavTabs" => "MyAccount/newModelNavTabs.html",
		"myAccountNavTabsModelsTab" => "MyAccount/myAccountNavTabsModelsTab.html",
		
		// ***********************************
		// Forms
		// ***********************************
		"loginForm" => "Forms/loginForm.html",
		"componentsForm" => "Forms/componentsForm.html",
		"newModelInfoForm" => "Forms/newModelInfoForm.html",
		"createAccountForm" => "Forms/createAccountForm.html",
		
		// ***********************************
		// View Model
		// ***********************************
		"viewModelNavTabs" => "ViewModel/viewModelNavTabs.html",
		"viewMyModelNavTabs" => "ViewModel/viewMyModelNavTabs.html",
		"viewModelNavTabsInfoTab" => "ViewModel/viewModelNavTabsInfoTab.html",
		"viewModelInfoEditButton" => "ViewModel/viewModelInfoEditButton.html",
		"viewModelNavTabsInfoTabEditable" => "ViewModel/viewModelNavTabsInfoTabEditable.html",
		"viewModelComponent" => "ViewModel/viewModelComponent.html",
		"viewMyModelComponent" => "ViewModel/viewMyModelComponent.html",
		"viewModelComponentsAccordion" => "ViewModel/viewModelComponentsAccordion.html",
		"viewModelComponentEditable" => "ViewModel/viewModelComponentEditable.html",
		"viewModelSpec" => "ViewModel/viewModelSpec.html",
		"viewModelSpecEditable" => "ViewModel/viewModelSpecEditable.html",
		"viewModelFeature" => "ViewModel/viewModelFeature.html",
		"viewModelFeatureEditable" => "ViewModel/viewModelFeatureEditable.html",
		
		// ***********************************
		// Common
		// ***********************************
		"infoAlert" => "Common/Alerts/infoAlert.html",
		"successAlert" => "Common/Alerts/successAlert.html",
		"errorAlert" => "Common/Alerts/errorAlert.html",
		"welcomeBanner" => "Common/welcomeBanner.html",
		"addHeader" => "Common/addHeader.html",
		"addParagraph" => "Common/addParagraph.html",
		"null" => "Common/null.html",
		
) );

global $db_user;
global $db_password;
global $db_host;
global $db_name;
$db = new database ();
$db->setup ( $db_user, $db_password, $db_host, $db_name );
$title = "";
/*
 * ******************* FUNCTIONS ********************
 */
function addHeader($str) {
	global $tpl;
	$tpl->assign ( "H1CONTENT", $str );
	$tpl->parse ( "HEADER", ".addHeader" );
}
function addParagraph($str) {
	global $tpl;
	$tpl->assign ( "CONTENT", $str );
	$tpl->parse ( "BODY", ".addParagraph" );
}
function addLoginForm() {
	global $tpl;
	$tpl->parse ( "LOGIN", ".loginForm" );
}
function addWelcomeBanner() {
	global $tpl;
	$tpl->assign ( "NICKNAME", $_SESSION ['nickname'] );
	$tpl->parse ( "LOGIN", ".welcomeBanner" );
}
function successMessage($title, $msg) {
	global $tpl;
	$tpl->assign ( "SUCCESSTITLE", $title );
	$tpl->assign ( "SUCCESSMESSAGE", $msg );
	$tpl->parse ( "BODY", ".successAlert" );
}
function errorMessage($title, $msg) {
	global $tpl;
	$tpl->assign ( "ERRORTITLE", $title );
	$tpl->assign ( "ERRORMESSAGE", $msg );
	$tpl->parse ( "BODY", ".errorAlert" );
}
function infoMessage($title, $msg) {
	global $tpl;
	$tpl->assign ( "INFOALERTTITLE", $title );
	$tpl->assign ( "INFOALERTMESSAGE", $msg );
	$tpl->parse ( "BODY", ".infoAlert" );
}
function setTitle($str) {
	global $title;
	$title = $str;
}
function display() {
	global $tpl;
	global $title;
	If (isLoggedIn ()) {
		addWelcomeBanner ();
	} else {
		addLoginForm ();
	}
	$tpl->assign ( "TITLE", $title );
	$tpl->parse ( "BODY", "main" );
	$tpl->FastPrint ();
}
function isLoggedIn() {
	return (isset ( $_SESSION ['email'] ) && trim ( $_SESSION ['email'] ) != "");
}

?>