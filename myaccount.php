<!-- 
* Author: Sean Watts
* Email: swatts@stevens.edu
* Date Created: 7/1/2015
-->

<?PHP
include ("includes/my_include.php");
session_start ();
setProfileActive ();
if (isCreatingNewAccount ()) {
	createNewUser ();
	setProfileActive ();
} else if (isset ( $_POST ['modelname'] ) && trim ( $_POST ['modelname'] ) != "") {
	$model_id = addNewModel ();
	if ($model_id >= 0) {
		$_SESSION ['addingModel'] = $model_id;
		successMessage ( "Model Added: ", "'" . $_POST ['modelname'] . "' has been added" );
		setModelsActive ();
	}
} else if (isLoggingIn ()) {
	login ( $_POST ['email'], $_POST ['password'] );
	setProfileActive ();
} else if (! isLoggedIn ()) {
	addHeader1 ( "Error!" );
	errorMessage ( "Not Logged In:", "You must be logged in to view this page." );
	display ();
	exit ();
}
setTitle ( "My Account" );
addHeader ( "Your Account" );

if (isset ( $_GET ['update'] )) {
	if ($_GET ['update'] = "location") {
		if (isset ( $_POST ['city'] ) && trim ( $_POST ['city'] ) != "") {
			updateInfo ( 'city', ucwords ( strtolower ( $_POST ['city'] ) ) );
		}
		if (isset ( $_POST ['state'] ) && trim ( $_POST ['state'] ) != "") {
			updateInfo ( 'state', ucwords ( strtolower ( $_POST ['state'] ) ) );
		}
		if (isset ( $_POST ['country'] ) && trim ( $_POST ['country'] ) != "") {
			updateInfo ( 'country', ucwords ( strtolower ( $_POST ['country'] ) ) );
		}
	}
	if ($_GET ['update'] = "account") {
		if (isset ( $_POST ['email'] ) && trim ( $_POST ['email'] ) != "") {
			updateInfo ( 'email', strtolower ( $_POST ['email'] ) );
		}
		if (isset ( $_POST ['nickname'] ) && trim ( $_POST ['nickname'] ) != "") {
			updateInfo ( 'nickname', ucwords ( strtolower ( $_POST ['nickname'] ) ) );
		}
		if (isset ( $_POST ['password'] ) && isset ( $_POST ['passwordConfirm'] ) && trim ( $_POST ['password'] ) != "") {
			if ($db->escape ( $_POST ['password'] ) == $db->escape ( $_POST ['passwordConfirm'] )) {
				updateInfo ( 'password', sha1 ( $_POST ['password'] ) );
				successMessage ( "Success: ", "Your password has been changed" );
			} else {
				errorMessage ( "Error: ", "Your passwords did not match. Password has not been changed" );
			}
		}
	}
	if (isset ( $_POST ["uploadImage"] )) {
		saveImg ();
	}
	setProfileActive ();
}

if (isset ( $_GET ['edit'] ) && $_GET ['edit'] == "account") {
	addMyAccountNavTabsAccountTabEditable ( $_SESSION ['email'], $_SESSION ['nickname'], getProfilePic () );
} else {
	addMyAccountNavTabsAccountTab ( $_SESSION ['email'], $_SESSION ['nickname'], getProfilePic () );
}
if (isset ( $_GET ['edit'] ) && $_GET ['edit'] == "location") {
	addMyAccountNavTabsLocationTabEditable ( $_SESSION ['city'], $_SESSION ['state'], $_SESSION ['country'] );
} else {
	addMyAccountNavTabsLocationTab ( $_SESSION ['city'], $_SESSION ['state'], $_SESSION ['country'] );
}

if (isset ( $_GET ['deletemodel'] ) && $_GET ['deletemodel'] != "") {
	deleteModel ( $_GET ['deletemodel'] );
	setModelsActive ();
}

addModelLinks ();
addMyAccountNavTabsModelsTab ();
addMyAccountNavTabs ();
display ();
/*
 * ******************* FUNCTIONS ********************
 */
function login($e, $p) {
	global $db;
	$sql = "SELECT user_id, nickname, city, state, country FROM users WHERE email = '" . $db->escape ( $e ) . "' AND password = '" . sha1 ( $db->escape ( $p ) ) . "';";
	$res = $db->send_sql ( $sql );
	if (mysqli_num_rows ( $res ) == 0) {
		addHeader ( "Email/Password was incorrect" );
		errorMessage ( "Error: ", "Double check that you're entering the correct Email/Password and try again" );
		display ();
		exit ();
	} else {
		$_SESSION ['email'] = $_POST ["email"];
		$row = $db->next_row ();
		$_SESSION ['id'] = $row [0];
		$_SESSION ['nickname'] = $row [1];
		$_SESSION ['city'] = $row [2];
		$_SESSION ['state'] = $row [3];
		$_SESSION ['country'] = $row [4];
	}
}
function saveImg() {
	$target_dir = "./UserData/" . $_SESSION ['id'] . "/profilepic/";
	$temp_name = $_FILES ["pic"] ["tmp_name"]; // get the temporary filename/path on the server
	$name = $_FILES ["pic"] ["name"]; // get the filename of the actual file
	$imageFileType = pathinfo ( $target_dir . $name, PATHINFO_EXTENSION );
	if ($_FILES ["pic"] ["size"] > 6000000) {
		errorMessage ( "File Size: ", "Sorry, the file you chose exceeds the 6Mb size limit." );
		return;
	}
	if (strtolower ( $imageFileType ) != "jpg" && strtolower ( $imageFileType ) != "png" && strtolower ( $imageFileType ) != "gif") {
		errorMessage ( "File Type: ", "Sorry, the file you chose is not a picture. Only JPG, PNG, and GIF formats are allowed." );
		return;
	}
	$oldProfilePic = getProfilePic ();
	move_uploaded_file ( $temp_name, $target_dir . $name );
	if ($oldProfilePic != "./images/emptyUser.png") {
		unlink ( $oldProfilePic );
	}
}
function getProfilePic() {
	$dir = "./UserData/" . $_SESSION ['id'] . "/profilepic/";
	$files = scandir ( $dir );
	foreach ( $files as $file ) {
		if (is_file ( $dir . $file )) {
			return $dir . $file;
		}
	}
	return "./images/emptyUser.png";
}
function createNewUser() {
	global $db;
	$sql = "SELECT email FROM users WHERE email = '" . $db->escape ( $_POST ['email'] ) . "';";
	$res = $db->send_sql ( $sql );
	if (mysqli_num_rows ( $res ) == 0) {
		// Add new user
		$sql = "INSERT INTO users (email, password, nickname, city, state, country) 
				VALUES ('" . strtolower ( $db->escape ( $_POST ['email'] ) ) . "', '" . sha1 ( $db->escape ( $_POST ['password'] ) ) . "', '" . ucwords ( strtolower ( $db->escape ( $_POST ['nickname'] ) ) ) . "',
						 '" . ucwords ( strtolower ( $db->escape ( $_POST ['city'] ) ) ) . "', '" . ucwords ( strtolower ( $db->escape ( $_POST ['state'] ) ) ) . "', '" . ucwords ( strtolower ( $db->escape ( $_POST ['country'] ) ) ) . "');";
		$res = $db->send_sql ( $sql );
		$sql = "SELECT user_id, email, nickname, city, state, country FROM users WHERE email = '" . $db->escape ( $_POST ['email'] ) . "';";
		$res = $db->send_sql ( $sql );
		$row = $db->next_row ();
		$_SESSION ['id'] = $row [0];
		$_SESSION ['email'] = $row [1];
		$_SESSION ['nickname'] = $row [2];
		$_SESSION ['city'] = $row [3];
		$_SESSION ['state'] = $row [4];
		$_SESSION ['country'] = $row [5];
		mkdir ( "./UserData/" . $row [0] . "/profilepic/", 0777, true );
	} else {
		addHeader1 ( "Error" );
		errorMessage ( "Duplicate: ", " The email address already exhists." );
		display ();
		exit ();
	}
}
function addModelLink($id, $name, $type, $description) {
	global $tpl;
	$tpl->assign ( "MYMODELLINKID", $id );
	$tpl->assign ( "MYMODELLINKNAME", $name );
	$tpl->assign ( "MYMODELLINKTYPE", $type );
	$tpl->assign ( "MYMODELLINKDESCRIPTION", $description );
	$tpl->assign ( "MYMODELIMAGE", getModelProfilePic($id) );
	$tpl->parse ( "MYACCOUNTMODELACCORDION", ".myAccountModelLink" );
}
function addModelLinks() {
	global $db;
	global $tpl;
	$query = "SELECT model_id, name, type, description FROM models WHERE author_id = '" . $_SESSION ['id'] . "';";
	
	$res = $db->send_sql ( $query );
	if (mysqli_num_rows ( $res ) != 0) {
		while ( $row = $db->next_row () ) {
			addModelLink ( $row [0], $row [1], $row[2], $row[3] );
		}
	$tpl->parse ( "MYMODELS", ".myAccountModelAccordion" );
	} else {
		infoMessage ( "No Models: ", " You have not created any models yet" );
		$tpl->assign ( "MYMODELS", "" );
	}
}
function rrmdir($dir) {
	if (is_dir ( $dir )) {
		$objects = scandir ( $dir );
		foreach ( $objects as $object ) {
			if ($object != "." && $object != "..") {
				if (filetype ( $dir . "/" . $object ) == "dir")
					rrmdir ( $dir . "/" . $object );
				else
					unlink ( $dir . "/" . $object );
			}
		}
		reset ( $objects );
		rmdir ( $dir );
	}
}
function deleteModel($id) {
	global $db;
	$query = "SELECT model_id, name FROM models WHERE author_id = '" . $_SESSION ['id'] . "' AND model_id = '" . $id . "';";
	$res = $db->send_sql ( $query );
	if (mysqli_num_rows ( $res ) != 0) {
		$row = $db->next_row ();
		$query = "DELETE FROM models WHERE model_id = '" . $id . "';";
		$res = $db->send_sql ( $query );
		rrmdir ( "./UserData/" . $_SESSION ['id'] . "/model_" . $id . "/" );
		successMessage ( "Model Deleted: ", " '" . $row [1] . "' has been deleted" );
	} else {
		errorMessage ( "HACKER: ", " You are attempting to delete a model that is not yours" );
	}
}
function addMyAccountNavTabs() {
	global $tpl;
	$tpl->parse ( "BODY", ".myAccountNavTabs" );
}
function addMyAccountNavTabsLocationTab($city, $state, $country) {
	global $tpl;
	$tpl->assign ( "CITY", $city );
	$tpl->assign ( "STATE", $state );
	$tpl->assign ( "COUNTRY", $country );
	$tpl->parse ( "MYACCOUNTPROFILETAB", ".myAccountNavTabsLocationTab" );
}
function addMyAccountNavTabsAccountTab($email, $nickname, $profilepic) {
	global $tpl;
	$tpl->assign ( "EMAIL", $email );
	$tpl->assign ( "NICKNAME", $nickname );
	$tpl->assign ( "PROFILEPIC", $profilepic );
	$tpl->parse ( "MYACCOUNTPROFILETAB", ".myAccountNavTabsAccountTab" );
}
function addMyAccountNavTabsAccountTabEditable($email, $nickname, $profilepic) {
	global $tpl;
	$tpl->assign ( "EMAIL", $email );
	$tpl->assign ( "NICKNAME", $nickname );
	$tpl->assign ( "PROFILEPIC", $profilepic );
	$tpl->parse ( "MYACCOUNTPROFILETAB", ".myAccountNavTabsAccountTabEditable" );
}
function addMyAccountNavTabsLocationTabEditable($city, $state, $country) {
	global $tpl;
	$tpl->assign ( "CITY", $city );
	$tpl->assign ( "STATE", $state );
	$tpl->assign ( "COUNTRY", $country );
	$tpl->parse ( "MYACCOUNTPROFILETAB", ".myAccountNavTabsLocationTabEditable" );
}
function addMyAccountNavTabsModelsTab() {
	global $tpl;
	$tpl->parse ( "NEWMODELFORM", ".newModelInfoForm" );
	$tpl->parse ( "MYACCOUNTMODELSTAB", ".myAccountNavTabsModelsTab" );
}
function isCreatingNewAccount() {
	return (isset ( $_GET ['newaccount'] ) && $_GET ['newaccount'] == "true");
}
function isLoggingIn() {
	if (isLoggedIn ()) {
		return false;
	}
	return (isset ( $_POST ['email'] ) && trim ( $_POST ['email'] ) != "" && isset ( $_POST ['password'] ) && trim ( $_POST ['password'] ));
}
function updateInfo($field, $newVal) {
	global $db;
	$sql = "UPDATE users SET " . $field . "='" . $newVal . "' WHERE email='" . $_SESSION ['email'] . "';";
	$res = $db->send_sql ( $sql );
	$_SESSION [$field] = $newVal;
}
function setProfileActive() {
	global $tpl;
	$tpl->assign ( "PROFILEACTIVE", "active" );
	$tpl->assign ( "PROFILETABACTIVE", "class=\"active\"" );
	$tpl->assign ( "MODELSACTIVE", "" );
	$tpl->assign ( "MODELSTABACTIVE", "class=\"enabled\"" );
	$tpl->assign ( "SETTINGSACTIVE", "" );
	$tpl->assign ( "SETTINGSTABACTIVE", "class=\"disabled\"" );
}
function setModelsActive() {
	global $tpl;
	$tpl->assign ( "PROFILEACTIVE", "" );
	$tpl->assign ( "PROFILETABACTIVE", "class=\"enabled\"" );
	$tpl->assign ( "MODELSACTIVE", "active" );
	$tpl->assign ( "MODELSTABACTIVE", "class=\"active\"" );
	$tpl->assign ( "SETTINGSACTIVE", "" );
	$tpl->assign ( "SETTINGSTABACTIVE", "class=\"disabled\"" );
}
function addNewModel() {
	global $db;
	// Add new model
	$sql = "SELECT * FROM `models` WHERE author_id='" . $_SESSION ['id'] . "' AND name='" . $db->escape($_POST ['modelname']) . "';";
	$res = $db->send_sql ( $sql );
	if (mysqli_num_rows ( $res ) == 0) {
		$sql = "INSERT INTO `models` (`model_id`, `author_id`, `name`, `type`, `timestamp`, `description`)
			 VALUES (NULL, '" . $_SESSION ['id'] . "', '" . $db->escape($_POST ['modelname']) . "', '" . $db->escape($_POST ['modeltype']) . "',
			 CURRENT_TIMESTAMP, '" . $db->escape($_POST ['modeldescription']) . "');";
		$res = $db->send_sql ( $sql );
		$newId = $db->insert_id ();
		mkdir ( "./UserData/" . $_SESSION ['id'] . "/model_" . $newId . "/profilePic", 0777, true );
		return $newId;
	} else {
		errorMessage ( "Duplicate Model", " You already have a model named '" . $_POST ['modelname'] . "', chose a different name" );
		return - 1;
	}
}
function getModelProfilePic($model_id) {
	$dir = "./UserData/" . $_SESSION ['id'] . "/model_" . $model_id . "/profilepic/";
	$files = scandir ( $dir );
	foreach ( $files as $file ) {
		if (is_file ( $dir . $file )) {
			return $dir . $file;
		}
	}
	return "./images/emptyModel.jpg";
}
?>