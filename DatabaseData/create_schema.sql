SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS `components` (
  `component_id` int(10) unsigned NOT NULL,
  `model_id` int(10) unsigned NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

INSERT INTO `components` (`component_id`, `model_id`, `name`) VALUES
(12, 8, 'Battery'),
(15, 10, 'Battery'),
(16, 10, 'Frame'),
(17, 10, 'Motor'),
(18, 13, 'Battery');

CREATE TABLE IF NOT EXISTS `features` (
  `feature_id` int(10) unsigned NOT NULL,
  `component_id` int(10) unsigned NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `features` (`feature_id`, `component_id`, `name`) VALUES
(2, 16, 'cf'),
(3, 17, 'abc');

CREATE TABLE IF NOT EXISTS `models` (
  `model_id` int(10) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL,
  `name` varchar(45) NOT NULL,
  `type` varchar(45) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `description` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `specs` (
  `spec_id` int(10) unsigned NOT NULL,
  `component_id` int(10) unsigned NOT NULL,
  `name` varchar(45) NOT NULL,
  `value` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(10) unsigned NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `nickname` varchar(45) NOT NULL,
  `city` varchar(45) NOT NULL,
  `state` varchar(45) NOT NULL,
  `country` varchar(45) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

ALTER TABLE `components`
  ADD PRIMARY KEY (`component_id`),
  ADD KEY `model_id` (`model_id`);

ALTER TABLE `features`
  ADD PRIMARY KEY (`feature_id`),
  ADD KEY `component_id` (`component_id`);

ALTER TABLE `models`
  ADD PRIMARY KEY (`model_id`),
  ADD KEY `author_id` (`author_id`);

ALTER TABLE `specs`
  ADD PRIMARY KEY (`spec_id`),
  ADD KEY `component_id` (`component_id`);

ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

ALTER TABLE `components`
  MODIFY `component_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;

ALTER TABLE `features`
  MODIFY `feature_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;

ALTER TABLE `models`
  MODIFY `model_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;

ALTER TABLE `specs`
  MODIFY `spec_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;

ALTER TABLE `users`
  MODIFY `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;

ALTER TABLE `components`
  ADD CONSTRAINT `components_ibfk_1` FOREIGN KEY (`model_id`) REFERENCES `models` (`model_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `features`
  ADD CONSTRAINT `features_ibfk_1` FOREIGN KEY (`component_id`) REFERENCES `components` (`component_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `models`
  ADD CONSTRAINT `models_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `specs`
  ADD CONSTRAINT `specs_ibfk_1` FOREIGN KEY (`component_id`) REFERENCES `components` (`component_id`) ON DELETE CASCADE ON UPDATE CASCADE;